const proxy = require("http-proxy-middleware");
module.exports = function expressMiddleware(router) {
  router.use(
    "/intervi",
    proxy({
      target: "http://localhost:9000",
      changeOrigin: true
    })
  );
};
