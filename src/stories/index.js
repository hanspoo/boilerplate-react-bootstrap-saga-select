import React from "react";
import { Provider } from "react-redux";

import { storiesOf } from "@storybook/react";
import store from "../redux/store";

import LectorUniversidadRegion from "../containers/LectorUniversidadRegion.js";

storiesOf("LectorUniversidadRegion", module) //
  .addDecorator(story => <Provider store={store}>{story()}</Provider>)
  .add("Extranjero", () => (
    <LectorUniversidadRegion universidad={null} region={null} />
  ));
