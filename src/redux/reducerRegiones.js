const INITIAL_STATE_REGIONES = {
  regiones: null,
  loading: true,
  error: ""
};

const reducerRegiones = (state = INITIAL_STATE_REGIONES, action) => {
  switch (action.type) {
    case "REGIONES_FETCH_REQUESTED":
      return INITIAL_STATE_REGIONES;
    case "REGIONES_FETCH_SUCCEEDED":
      return { loading: false, error: "", regiones: action.payload };
    case "REGIONES_FETCH_FAILED":
      return { loading: false, error: action.payload, regiones: null };
    default:
      return state;
  }
};

export default reducerRegiones;
