import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";

// worker Saga: will be fired on REGIONES_FETCH_REQUESTED actions
function* fetchRegiones(action) {
  console.log(`en saga, acción ${action.type}`);
  try {
    const response = yield call(() => axios.get("/intervi/regiones"));
    yield put({ type: "REGIONES_FETCH_SUCCEEDED", payload: response.data });
  } catch (e) {
    yield put({ type: "REGIONES_FETCH_FAILED", payload: e.message });
  }
}

function* sagaRegiones() {
  yield takeLatest("REGIONES_FETCH_REQUESTED", fetchRegiones);
}

export default sagaRegiones;
