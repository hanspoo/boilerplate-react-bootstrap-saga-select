import React from "react";
import { FormGroup, Radio } from "react-bootstrap";
import Select from "react-select";
import { number } from "prop-types";

export default class LectorUniversidadRegion extends React.Component {
  static propTypes = {
    region: number
  };

  state = {
    selectedRegion: null,
    origen: "nacional"
  };

  handleChange = selectedRegion => {
    this.setState({ selectedRegion });
    console.log(`Option selected:`, selectedRegion);
  };

  toggleOrigen = e => {
    this.setState({ origen: e.target.value });
  };

  render() {
    const { regiones } = this.props;

    const regsMapeadas = regiones.map(({ id, nombre }) => {
      return {
        value: id,
        label: nombre
      };
    });

    const { selectedRegion, origen } = this.state;

    const esNacional = origen === "nacional";

    return (
      <div style={styles.container}>
        <h3>¿ Donde estudió ?</h3>

        <Radio
          name="origen"
          value="nacional"
          checked={esNacional}
          onClick={this.toggleOrigen}
        >
          <b>En Chile</b>
        </Radio>

        <div style={{ paddingLeft: 20 }}>
          <FormGroup disabled controlId="formBasicText" validationState={false}>
            <div>Región</div>
            <Select
              id="select-region"
              isDisabled={!esNacional}
              value={selectedRegion}
              onChange={this.handleChange}
              options={regsMapeadas}
            />
          </FormGroup>
        </div>

        <Radio
          onClick={this.toggleOrigen}
          name="origen"
          value="extranjero"
          checked={origen === "extranjero"}
        >
          <b>En el extranjero</b>
        </Radio>
      </div>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
};
