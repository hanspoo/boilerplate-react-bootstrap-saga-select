import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";
import LectorUniversidadRegion from "./containers/LectorUniversidadRegion.js";

ReactDOM.render(
  <LectorUniversidadRegion universidad={43} region={null} />,
  document.getElementById("root")
);

const ele = document.getElementById("lee-universidad-region-estudio");
if (ele) {
  const universidad = parseInt(ele.getAttribute("data-universidad"), 10);
  const region = parseInt(ele.getAttribute("data-region"), 10);

  ReactDOM.render(
    <LectorUniversidadRegion universidad={universidad} region={region} />,
    ele
  );
}

registerServiceWorker();
