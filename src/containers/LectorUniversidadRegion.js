import React from "react";
import { connect } from "react-redux";
import { number } from "prop-types";
import LectorUniversidadRegion from "../components/LectorUniversidadRegion.js";

class ContainerLectorUniversidadRegion extends React.Component {
  static propTypes = {
    region: number
  };

  componentDidMount() {
    this.props.cargarRegiones();
  }

  render() {
    const { regiones, loading, error } = this.props.reducerRegiones;

    if (error) return <p>Error al cargar regiones {error}</p>;
    if (loading) return <p>Cargando...</p>;

    if (!regiones) return <p>Las regiones deberían haberse cargado...</p>;

    return <LectorUniversidadRegion {...this.props} regiones={regiones} />;
  }
}

const s2p = state => {
  return {
    reducerRegiones: state.reducerRegiones
  };
};

const d2p = dispatch => {
  return {
    cargarRegiones: () => dispatch({ type: "REGIONES_FETCH_REQUESTED" })
  };
};
export default connect(
  s2p,
  d2p
)(ContainerLectorUniversidadRegion);
